const express = require('express')
const https = require('https')
const app = express()
const port = 3000

app.get('/search', (req, res) => {
    let title = req.query.title || '';
    let artist = req.query.artist || '';

    if (title === '' && artist === '') {
        res.error({
            message: 'You must give at list a title or an artist, idiot.'
        });
    }

    const request = https.get('https://some-random-api.ml/lyrics/?title=' + escape(title + ' ' + artist), (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            let json = JSON.parse(data);
            json.musicWords = getWords(json.lyrics);
            console.log(json.title, json.author);
            res.json(json);
        });
    }).on('error', error => {
        console.error(error);
    });
});

const getWords = (lyrics) => {
    let array = [];

    lyrics.split('\n').forEach(line => {
        if (line !== '') {
            line.split(',').forEach(words => {
                words = words.split(' ').filter(n => !!n);
                if (!!words[words.length - 1]) {
                    array.push(words[words.length - 1]);
                }
            });
        }
    });

    return array;
};

app.use('/', express.static('./'));

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

// https://some-random-api.ml/lyrics/?title=Pelot%20d%27HennebontTri%20Yann
